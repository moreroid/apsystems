#!/bin/bash
set -euo pipefail
# SPDX-License-Identifier: GPL-3.0-only
#
# download helper for APsystems inverter and ecu firmwares
#
#
# download server/port
#
#readonly DLSERVER=ecueu.apsema.com/9220
readonly DLSERVER=ecuna.apsema.com/9220
#readonly DLSERVER=ecu.apsema.com/9220
#readonly DLSERVER=ecu2.apsema.com/9220

#
# inverter
#
# 404 YC500
# 406 YC600 (Europe, Middle East and Africa)
# 407 YC600-T (US and Canada)
# 408 YC600 (Europe, Middle East and Africa)
# 409 YC600-Y (US and Canada)
# 501 YC1000 (Europe, Middle East and Africa)
# 502 YC1000 (Europe, Middle East and Africa)
# 503 YC1000-3 (US and Canada)
# 504 YC1000-3 (US and Canada)
# 706 DS3D
# 801 QS1 (Europe, Middle East and Africa)
# 802 QS1 (US and Canada)

# DS3 firmware versions 2023.01.17
# DS3-S  702000000001-702000002000 firmware version "002284"
#        702000002001-702000049500 firmware version "003067"
# DS3-L  703000000001-703000042262 firmware version "002284"
#        703000052173-703000084462 firmware version "003067"
# DS3    704000000001-704000000500 firmware version "002284"
#        704000000501-704000049000 firmware version "003067"
# DS3-H? 705000000001-705000004690 firmware version "003067"

#readonly INVERTER=YC600
#readonly INVERTER=YC1000
#readonly INVERTER=QS1
readonly INVERTER=DS3 # 2022-12-08 ecuna: update68 (first update67 last update73)
#readonly INVERTER=DS3D
#readonly INVERTER=QT2D

#
# Energy Communication Unit (ECU)
#
# ECU-3  203             ECU-3 PLC version 3 (V3) for YC500
# ECU-3  204             ECU-3 PLC version 4 (V4) for YC500
# ECU-3Z 213 21300019948 ECU-3Z ZigBee version 3 (V3) for YC1000 (not compatible with YC600 & QS1)
# ECU-4Z 214             ECU-3Z ZigBee version 4 (V4) for YC1000 (not compatible with YC600 & QS1)
# ECU-C  215
# ECU-B  2163
# ECU-R  2162

# 202?-xx-xx ECU-B v1.2.17M6 (oldest known)
#readonly DLURI=/ECU_B/V1.2.15/ecu-b.bin
#readonly DLNAME=ecu_b-v1.2.15.bin

# 202?-xx-xx ECU-B v1.2.19
#readonly DLURI=/ECU_B/V1.2.17M6/ecu-b.bin
#readonly DLNAME=ecu_b-v1.2.17M6.bin
#or
#readonly DLURI=/ECU_B/V1.2.17/ecu-b.bin
#readonly DLNAME=ecu_b-v1.2.17.bin

# 202?-xx-xx ECU-B v1.2.20
#readonly DLURI=/ECU_B/V1.2.19/ecu-b.bin
#readonly DLNAME=ecu_b-v1.2.19.bin

# 202?-xx-xx ECU-B v1.2.21
#readonly DLURI=/ECU_B/V1.2.20/ecu-b.bin
#readonly DLNAME=ecu_b-v1.2.20.bin

# 2022-xx-xx ECU-B v1.2.22
#readonly DLURI=/ECU_B/V1.2.21/ecu-b.bin
#readonly DLNAME=ecu_b-v1.2.21.bin

# 2022-xx-xx ECU-B v1.2.23
#readonly DLURI=/ECU_B/V1.2.22/ecu-b.bin
#readonly DLNAME=ecu_b-v1.2.22.bin

# 2023-01-xx ECU-B v1.2.25
#readonly DLURI=/ECU_B/V1.2.23/ecu-b.bin
#readonly DLNAME=ecu_b-v1.2.23.bin

# 2023-01-xx ECU-B v1.2.25A
#readonly DLURI=/ECU_B/V1.2.25/ecu-b.bin
#readonly DLNAME=ecu_b-v1.2.25.bin

# TODO 2023-xx-xx ECU-B v1.2.xx
#readonly DLURI=/ECU_B/V1.2.25A/ecu-b.bin
#readonly DLNAME=ecu_b-v1.2.25A.bin

# extra inverter firmware since ECU-B v1.2.25
# possible for inverters:
# 702000221357-702000287356
# 703000350273-703000442272
# 703000462273-703000492272
# 704000102801-704000136800
# 705000004691-705000005190
# 707000000001-707000010400
#readonly DLURI=/emergency/DS3_280025_20221114_Rev1_V5_T612.BIN
#readonly DLNAME=DS3_280025_20221114_Rev1_V5_T612.BIN

readonly SAVEFILE_PREFIX=firmware-apsystems
declare -r -A MODEL=([YC600]=01 [YC1000]=02 [QS1]=03 [DS3]=04 [DS3UNKNOWN]=05 [DS3D]=06 [DS3D]=07 [UNKNOWN]=08 [QT2D]=09)

exit_error() {
    echo "ERROR:$0:$*"
    exit 1
}
read8() {
    local _r8_var=$1 _r8_car LANG=C IFS=
    read -r -d '' -n 1 -u 3 _r8_car
    printf -v "$_r8_var" %d \'"$_r8_car"
}
read16() {
    local _r16_var=$1 _r16_lb _r16_hb
    read8 _r16_hb && read8 _r16_lb
    printf -v "$_r16_var" %d $(( _r16_hb<<8 | _r16_lb ))
}
read32() {
    local _r32_var=$1 _r32_lw _r32_hw
    read16 _r32_hw && read16 _r32_lw
    printf -v "$_r32_var" %d $(( _r32_hw<<16| _r32_lw ))
}
num_to_fourbyte() {
    local -r _num=$1
    local -r _hex=$(printf -- %.8x "${_num}")
    for ((i=0; i <${#_hex}; i=i+2)); do printf %b "\x${_hex:i:2}" ; done
}
bashseq() {
    local _end=$1
    local _i
    typeset -i i _end # Let's be explicit
    for ((i=1;i<=_end;++i)); do echo "$i"; done
}
# Unfortunately, as bash strings cannot hold null bytes ($\0), reading one byte once is required.
readN() {
    local i=$1
    local -r savenull=${2:-}
    local LANG=C IFS= _onebyte
    for ((;i--;)) {
        read -r -d '' -n 1 -u 3 _onebyte
        [[ ${#_onebyte} == 0 ]] && [[ ${savenull} ]] && _onebyte="\x00"
        echo -en "${_onebyte}"
    }
}
tcp_open() {
    exec 3<>/dev/tcp/"${DLSERVER}" # open TCP socket
}
tcp_close() {
    exec 3<&- # close TCP socket
}
#send
#00000000  45 43 55 31 31 30 30 33 32 30 30 30 34 32 31 36  ECU1100320004216
#00000010  33 30 30 30 39 39 39 39 39 30 30 30 34 45 4e 44  3000999990004END
#recv
#00000000  45 43 55 31 31 30 30 39 39 30 30 30 34 32 31 36  ECU1100990004216
#00000010  33 30 30 30 39 39 39 39 39 30 30 30 34 45 4e 44  3000999990004END
#00000020  31 2f 49 6e 76 65 72 74 65 72 2f 41 75 74 6f 6d  1/Inverter/Autom
#00000030  61 74 69 63 20 55 70 64 61 74 65 73 2f 44 53 33  atic Updates/DS3
#00000040  2f 75 70 64 61 74 65 35 34 2e 74 78 74 00 00 00  /update54.txt...
#00000050  00 00 00 00 00 00 00 00 00 00 00 00 00 30 30 35  ........ .....005
#00000060  45 4e 44                                          END
dl_get_update_latest_uri() {
    local -r _model=$1

    local -r _model_type=${MODEL[${_model}]}
    local _response_cmd _response_uri

    tcp_open
    echo -n "ECU110032000421630009999900${_model_type}END" >&3
    _response_cmd=$(readN 33)
    [[ "${_response_cmd}" == ECU110099000421630009999900${_model_type}END1 ]] || exit_error "unexpected response cmd: \"${_response_cmd}\""
    _response_uri=$(readN 60)
    [[ $(readN 6) == "005END" ]] || exit_error "unexpected tag end"
    echo "${_response_uri}"
    tcp_close
}
#00000000  45 43 55 31 31 30 30 38 30 30 30 30 31 30 30 30  ECU1100800001000
#00000010  31 2f 45 43 55 5f 42 2f 56 31 2e 32 2e 32 31 2f  1/ECU_B/V1.2.21/
#00000020  65 63 75 2d 62 2e 62 69 6e 00 00 00 00 00 00 00  ecu-b.bin.......
#00000030  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................
#00000040  00 00 00 00 00 00 00 00 00 00 00 00 00 45 4e 44  .............END
dl_info_req() {
    local -r _file=$1
    echo -en "ECU11008000010001${_file}"
    # shellcheck disable=SC2046
    printf -- "\x00%.0s" $(bashseq $((60 - ${#_file})))
    echo -n 'END'
}
#00000000  45 43 55 31 31 30 30 33 30 30 30 30 31 30 30 30  ECU1100300001000
#00000010  31 45 4e 44 31 00 04 94 40 17 f7 45 4e 44        1END1...@..END
dl_info_response() {
    local -r _dlfile=$1
    local -r _filesize_var=$2
    local -r _cmd=ECU11003000010001END1
    local _filesize _crc _responsecmd
    read -r -N "${#_cmd}" -u 3 _responsecmd
    [[ "${_responsecmd}" == ECU11003000010001END0 ]] && exit_error "dlfile ${_dlfile} not found"
    [[ "${_responsecmd}" == "${_cmd}" ]] || exit_error "unexpected response cmd: ${_responsecmd}"
    read32 _filesize
    read16 _crc
    [[ $(readN 3) == "END" ]] || exit_error "unexpected dl info end"
    printf -v "${_filesize_var}" %d "${_filesize}"
}
#00000000  45 43 55 31 33 30 30 38 34 30 30 30 32 2f 45 43  ECU1300840002/EC
#00000010  55 5f 42 2f 56 31 2e 32 2e 32 31 2f 65 63 75 2d  U_B/V1.2.21/ecu-
#00000020  62 2e 62 69 6e 00 00 00 00 00 00 00 00 00 00 00  b.bin...........
#00000030  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  ................
#00000040  00 00 00 00 00 00 00 00 00 00 00 00 01 00 00 03  ................
#00000050  e4 45 4e 44                                      .END
dl_chunk_req() {
    local -r _file=$1
    local -r _counter=$2
    local -r _chunksize=$3
    echo -en "ECU1300840002${_file}"
    # shellcheck disable=SC2046
    printf -- '\x00%.0s' $(bashseq $((60 - ${#_file}))) 
    num_to_fourbyte "${_counter}"
    num_to_fourbyte "${_chunksize}"
    echo -n 'END'
}
# 00000000  45 43 55 31 33 31 30 32 32 30 30 30 32 30 31 00  ECU131022000201.
# 00000010  00 00 01 03 e4 98 ff 00 20 b9 82 00 08 97 45 01  ........ .....E.
# ...
dl_chunk_read() {
    local _dlsavefile=$1
    local _response_cmd _counter _size _crc
    # ECU130028000201 # size 2
    # ECU130126000201 # size 100
    # ECU131026000201 # size 1000
    read -r -N 15 -u 3 _response_cmd
    if [[ "${_response_cmd}" == ECU13*000201 ]] ; then
        local -r _chunksize=$(("${_response_cmd:5:4}" - 26))
    # ECU130086000200 # end chunk
    elif [[ "${_response_cmd}" == ECU13*000200 ]] ; then
        local _chunksize=$((10#${_response_cmd:5:4}))
        _chunksize=$(("${_chunksize}" - 26))
    else
        exit_error "unexpected dl response cmd: \"${_response_cmd}\""
    fi
    read32 _counter
    read16 _size
    echo "chunk:${_counter} chunksize: ${_chunksize} payloadsize:${_size}"
    readN "${_size}" savenull >> "${_dlsavefile}"
    read16 _crc
    [[ $(readN 3) == "END" ]] || exit_error "unexpected dl tag end"
}
dl() {
    local -r _dlfile=$1
    local -r _savefile=$2
    local -r _chunksize=1000 # max 1000 supported, sniff using 996

    tcp_open

    local _dlsize
    dl_info_req "${_dlfile}" >&3
    dl_info_response "${_dlfile}" _dlsize

    local _chunkcount=$((_dlsize / _chunksize))
    [[ $((_dlsize % _chunksize)) == 0 ]] || _chunkcount=$((_chunkcount+1))
    echo "dlfile: ${_dlfile} size:${_dlsize} chunks:${_chunkcount}"

    [[ -r "${_savefile}" ]] && exit_error "save file ${_savefile} exist already"
    for ((c=1;c<=_chunkcount;c++)) {
        dl_chunk_req "${_dlfile}" "${c}" "${_chunksize}" >&3
        dl_chunk_read "${_savefile}"
    }
    tcp_close
    echo "success download"
}
analyze_update_file() {
    local -r _update_file=$1
    local _line _model _version _id_start _id_end _uri _savefile
    while read -r _line ; do
        [[ ${_line} =~ ^MODEL:([A-Z0-9]+) ]] && _model=${BASH_REMATCH[1]}
        [[ ${_line} =~ ^VERSION:([0-9]+) ]] && _version=${BASH_REMATCH[1]}
        [[ ${_line} =~ ^ID_START:([0-9]+) ]] && _id_start=${BASH_REMATCH[1]}
        [[ ${_line} =~ ^ID_END:([0-9]+) ]] && _id_end=${BASH_REMATCH[1]}
        if [[ ${_line} =~ ^Package_Path[0-9]*:([^$'\r']+) ]] ; then
            _uri="${BASH_REMATCH[1]}"
            _savefile="${SAVEFILE_PREFIX}-${_model}-${_version}-${_id_start}-${_id_end}-${_uri##*/}"
            dl "${_uri}" "${_savefile}"
        fi
    done < <(cat "${_update_file}" ; echo)
}
dl_inverter_update() {
    local -r _model=$1
    local -r _updateversion_force=${2:-}

    if [[ "${_updateversion_force}" ]] ; then
        local -r _dluri="/Inverter/Automatic Updates/${INVERTER}/update${_updateversion_force}.txt"
    else
        local -r _dluri=$(dl_get_update_latest_uri "${_model}")
    fi
    local -r _savefile="${SAVEFILE_PREFIX}-${_model}-${_dluri##*/}"
    echo "download \"${_dluri}\" to file \"${_savefile}\""
    dl "${_dluri}" "${_savefile}"
    analyze_update_file "${_savefile}"
}
# main
# for inverter
dl_inverter_update "${INVERTER}" "${1:-}"
# for ecu
#dl "${DLURI}" "${SAVEFILE_PREFIX}-${DLNAME}"
exit 0
# EOF